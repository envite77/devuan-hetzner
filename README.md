# Devuan Hetzner

How to migrate Hetzner machines to Devuan

- © Noel Torres, 2021
- This file and all files in this project are under the GNU GPL v2.0 License. See the file LICENSE for more information.

The objective of this Project is to show how to perform the migration of [Hetzner](https://www.hetzner.com/) (a well known, cheap, Linux hosting provider) Debian machines to Devuan.

At the moment, the following transitions have been tested:

1. Debian 9 Stretch to Devuan 3 Beowulf
